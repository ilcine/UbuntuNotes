# MySql 

    MySQL provides access to Tables, Views, Procedures, Triggers, Cursors that it maintains on server systems, establishes relationships between tables; Provides necessary index engines for the database, such as transection, subqueries. 
    After getting MySQL from Oracle, the project continues with the name of MariaDB.

> _Emrullah İLÇİN_ 

## Installation (MySql or MariaDB)

* `apt-get -y install mysql-server-5.7`  # for MySql
* `apt -y install mariadb-server`  # for MariaDB

<pre>
  systemctl restart mariadb
    Change the root password? [Y/n] y
    Remove anonymous users? [Y/n] y  # remove ananoim users.
    Disallow root login remotely? [Y/n]  # remote access to root.
    Remove test database and access to it? [Y/n] y
    Reload privilege tables now? [Y/n] y
</pre>

* `mysql --version* ` # See versiyon

* first login; `mysql -uroot -p` # Mysql database is connected to first root user; command prompt is `mysql>`

## Create user and database
ex; database name is `proje1`; user name is `emr`; `project1` will be authorized to access from DB, localhost and external connections. (user:emr,password:123456); use `flush privileges` for configin to be active.

* _While on console_ 

```
sudo mysql -uroot -p -e "create database proje1"; 
sudo mysql -uroot -p -e "grant all on proje1.* to $USER@localhost identified  by '123456';"
sudo mysql -uroot -p -e "grant all on proje1.* to $USER identified by '123456' WITH GRANT OPTION;"
sudo mysql -uroot -p -e "flush privileges;"
```

* _At the "mysql>" prompt_

```
create database proje1; 
grant all on proje1.* to $USER@localhost identified by '123456';
grant all on proje1.* to $USER identified by '123456' WITH GRANT OPTION;
flush privileges;
exit;
```
* _Connect to db with user_ 

`mysql -uemr -p proje1`  # -u:user; -p password; proje1:database name; comes prompt when connected
 `mysql>`; for exit `exit;`


* _Other examples of authorization on mysql prompt_

> `create database proje1 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;` # Define "CHARACTER SET" if needed

> `grant all on proje1.* to emr identified  by "123456" WITH GRANT OPTION;` # if `emr` user will connect to DB server from all IPs

> `grant all on proje1.* to 'emr'@'1.2.3.4' identified  by "123456" WITH GRANT OPTION;`  # User `emr`, only if connecting from IP number 1.2.3.4

> Dış erişime açma:  proje1 ile DB farklı sunucularda ise proje1 in IP sini DB'e tanıtmak gerekir. `/etc/mysql/mysql.conf.d/mysqld.cnf` ta sadece kendi sunucusundan bağlan ifadesini yorumla `#bind-address = 127.0.0.1` ve mysql i restart yap `/etc/init.d/mysql restart` ve  Erişim etkisi ver;

> Remove all privileges; `REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'emr'@'%';`

> See the privileges granted; `SELECT * from information_schema.user_privileges where grantee like "'emr%";`

* _Other info commands_

```
show databases; # see DBs
show tables; # te Tables
desc mysql.user;  # see the authority
select host, user from mysql.user;  # See assigned hosts
show variables like "character_set_database"; # default character set is utf8mb4
show variables like 'char%';
show character set; 
show character set WHERE CHARSET = 'utf8';
exit; # 
```

## Strict definition:
Strict SQL Mode controls how it handles invalid, missing or ambiguous values ​​to data exchange tables, if there are uncertainties in data entries, it returns null. MySql version 5.7 after "strict mode" comes true. <small>Strict SQL Modu veri değişim tablolara geçersiz, eksik veya belirsiz değerleri nasıl işleyeceğini kontrol eder, veri girişlerinde belirsizlikler varsa bu null olarak döndürür. MySql version 5.7 den sonra "strict mode" true olarak gelir.</small> [Strict Mode Link](https://dev.mysql.com/doc/refman/5.7/en/sql-mode.html#sql-mode-strict)

* _strict mode display_

`mysql> SELECT @@sql_mode;` # ile veya `SHOW VARIABLES LIKE 'sql_mode';` # see strict mode; mode is active if there is  'STRICT_TRANS_TABLES' and 'ONLY_FULL_GROUP_BY'; if not, it is passive

### _To permanently disable or enable "strict mode" in Ubuntu 16.04_ <small>Ubuntu'da "strict mode"'yi kalıcı olarak devre dışı bırakmak veya bırakmamak için</small> 

* _For strict mode enable_

"/etc/mysql/conf.d/disable_strict_mode.cnf"  // edit or create and insert this. // and restart mysql 

<pre>
[mysqld]
#sql_mode=IGNORE_SPACE,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
</pre>

* _For strict mode disable_

<pre>
/etc/mysql/conf.d/disable_strict_mode.cnf  //delete strict_mode line or file // and restart mysql 
</pre>

## MySql backup

* export all mysql data in `db_name.sql` files; The `-R` flag is to make sure stored procedures and functions are included in the dump file

`mysqldump -u username -ppassword -R db_name > db_name.sql`

* export mysql data;  for mysql console; excluding insert record; 
`mysqldump --skip-extended-insert -uemr -p şifre DataBaseName table1 table2 > database.sql` # table1 and table2 backup only

* export mysql data; for linux command prompt
`mysqldump -uemr -p şifre --no-data Databaseadi > muh.structure.sql`  # structure export only

--skip-add-drop-table // does not take table names
--no-create-info  // does not write table names as "create"
--all-databases // takes all the tables

* import to MySql with mysql console
mysql -uemr -p şifre DB < muh.sql

* Uploading "CSV format" data to mysql (import) <small>MySql'e csv desenli veri yükleme </small>

1) create table in mysql, give column names, ex: a1,a2,a3 vb
2) create file in etc directory; ex "/tmp/table1.csv", Let the structure be like "1",2,3,4 
3) `LOAD DATA INFILE '/tmp/table1.csv' INTO TABLE table1 CHARACTER SET UTF8 FIELDS TERMINATED BY ',' ENCLOSED BY '"';`

* Download "CSV format" data from mysql (export)

`select * from table1  INTO OUTFILE '/tmp/table1.txt' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n';`
  
* Running mysql commands in the linux consol
 
`mysql -uemr -p şifre -hlocalhost -Bse 'show databases'` # tüm db leri görme

`mysql -uemr -p şifre -hlocalhost -Bse 'show tables'` # tüm tabloları görme

* Delete mysql database

`drop database proje1;`

## For Laravel:

> notes 1: The default of strict mode in Laravel is true, true means high security and forces you to write correct queries. For some queries, it may be desirable to ignore accuracy. For example: If we want to "group by" according to the constantly changing time field, the strict mode returns an error, in this case Strict mode is false for this query. <small>Laravel'de strict mode'u nun default hali true dir, true olması güvenlik yüksek demektir ve sizi doğru sorgular yazmaya zorlar. Kimi sorgularda doğruluk gözardı edilmek istenebilir ör:sürekli değişen time alanına göre "group by" yapmak istersek strict mode hata döndürür bu durumda bu sorgu için Strict modu false yapılır.</small>

> notes 2: Stict mode can also be turned off from within Laravel. Make the following change in config file `config/database.php` and  `'strict' => false,` 

> notes 3: For "storage procedure" use `SET SESSION sql_mode = "";`

> notes 4: Other "sql_mode" in Laravel;  ex: To turn off "Ğroup By"; edit  `config/database.php` and write this

```
'modes' => [
            //'ONLY_FULL_GROUP_BY', // Disable this to allow grouping by one column
            'STRICT_TRANS_TABLES',
            'NO_ZERO_IN_DATE',
            'NO_ZERO_DATE',
            'ERROR_FOR_DIVISION_BY_ZERO',
            'NO_AUTO_CREATE_USER',
            'NO_ENGINE_SUBSTITUTION'
],
```

notes 5: To enable strict mods in Laravel

```
'connections' => [
        'mysql' => [
            // Ignore this key and rely on the strict key
            'modes' => null,

            // Explicitly disable all modes, overriding strict setting
            'modes' => [],

            // Explicitly enable specific modes, overriding strict setting
            'modes' => [
                'STRICT_TRANS_TABLES',
                'ONLY_FULL_GROUP_BY',
            ],
        ]
    ]
```

notes 6: If the MySql version is less than 5.4

Laravel `SQLSTATE[42S01]` şeklinde bir hata döndürür. Düzeltmek için alttaki değişikliği yap veya mysql’i  versiyon 5.4 ün üstüne güncelle.

joe ./app/Providers/AppServiceProvider.php  // editör ile bağlan

```
use Illuminate\Support\Facades\Schema;  // use yi tanımla 
public function boot()
    {
     Schema::defaultStringLength(191);
    }
```

### Laravel model usage 

* for index
$posts = DB::table(DB::raw('table1 force index(yevsira_index)')) // There are little differences "use index" and "force index"; index modes use "laravel query builder modes"

* for in app (ex: SomeModel.php)

```
Class SomeModel extends Model {

    public static function IndexRaw($index_raw)
    {
        $model = new static();
        $model->setTable(\DB::raw($model->getTable() . ' ' . $index_raw));
        return $model;
    }
    ...
}
```

`SomeModel::IndexRaw('FORCE INDEX (some_index_name)')->where('condition','=',true)->get();`

* index with query method

``` 
 $query = SomeModel::where()->with()...->orderBy();

    $query->getQuery()->from(\DB::raw($query->getQuery()->from . ' FORCE INDEX (index_name)'));
    // or just
    $query->getQuery()->from(\DB::raw('`table` FORCE INDEX (index_name)'));

    $results = $query->get();
```
 
