# Apache and PHP with Laravel

Install php on Ubuntu and define in apache2 web server

> _Emrullah İLÇİN_

## If Apache2 is not installed:

* `sudo apt install apache2`

## If Php is not installed: (Required example for Laravel)
    sudo apt install php7.2  php7.2-mysql php7.2-json php7.2-curl php7.2-cli php7.2-common php7.2-mcrypt php7.2-gd libapache2-mod-php7.2 php7.2-zip php7.2-pdo php7.2-mbstring php7.2-tokenizer php7.2-xml php7.2-fileinfo


## For Apache2
    sudo a2enmod php7.2
    sudo a2enconf php7.2-cgi
    sudo apt install libapache2-mod-php7.2
    sudo a2enconf php7.2-cgi
    sudo a2enconf php7.2-fpm
    sudo a2dismod mpm_event
    sudo a2enmod mpm_prefork
    sudo a2enmod rewrite   // enable write mode

    sudo vi /etc/php/7.2/apache2/php.ini #  change time  ## `;` remove any comment if any
          date.timezone = "Europe/Istanbul"
    
    sudo systemctl restart apache2  # apache restart  
    
> time laravel içinde de tanımlanabilir.  zaman php'de tanım yapmaya gerek kalmaz.

> `php -r "echo date_default_timezone_get();"` # timezone test.   

> apache'nin config, modul, envvars gibi parametreleri `/etc/apache2/` dizini altındadır. 

## php upgrade 7.1 to 7.2

* Remove previous packages

```
apt purge libapache2-mod-php7.1
apt purge php7*
apt purge php-*
apt purge phpunit*
apt autoremove
a2dismod php7.1
```

 * later `service apache2 restart` and install php7.2 packages
    
> `update-alternatives --set php /usr/bin/php7.2` # set 71 to 7.2 in php.ini

## See versiyon

<pre>
php --ini|more

or 

php -i | grep  "enabled"| grep  "BCMath\|ctype\|fileinfo\|json\|mbstring\|OpenSSL\|PDO\|Tokenizer\|XML"

or 

dpkg --list | grep php | awk '/^ii/{ print $2}' # see for full php packages

</pre>

or

> `apt-cache search php7.3`  # search php7.3 

> Install if feature is not for Laravel; ex: `apt install php7.2-mbstring`; 

> Openssl comes installed, if not installed, install it `apt-get install openssl` and add php.ini file `extension=openssl`

> `php -r 'phpinfo();' | php 2>&1 |grep -i ssl` # test ssl in php

## For php7.3 on laravel

```
apt install php7.3-common  # includes ctype,pdo,tokenizer
apt install php7.3-bcmath
apt install php7.3-json
apt install php7.3-mbstring
apt install php7.3-xml
apt install php7.3-mysql
apt install php7.3-zip
apt install php7.3-fileinfo
apt install openssl
php -i | grep -i openssl
```


## Test 
    php -v #  see version 
    php -r 'print Date("d/m/Y h:i:sa");' # see date and time 
    

> Restart apache2 when new feature is installed

. `systemctl restart apache2`
