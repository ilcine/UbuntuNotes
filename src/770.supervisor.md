# Supervisor

Runs automatically

_emr_

## Install

ex; Run Webscockets on emr user

```
apt install supervisor
systemctl enable supervisor
systemctl restart supervisor.service
```

## Config
cat > /etc/supervisor/conf.d/websockets.conf

```
[program:websockets]
command=/usr/bin/php /home/emr/wwwServer/laravel-websockets-chat/artisan websockets:serve
numprocs=1
autostart=true
autorestart=true
;user=laravel-echo
user=emr

[supervisord]
;remark(aciklama )
minfds=10240; 
;minfds aciklama:: (min. avail startup file descriptors;default 1024) maximim acabileceği dosya sayisini belirle
;logfile=/var/log/supervisor/supervisord.log ; (main log file;default $CWD/supervisord.log)
;pidfile=/var/run/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
;childlogdir=/var/log/supervisor            ; ('AUTO' child log dir, default $TEMP)

[inet_http_server]
; inet (TCP) server disabled by default
;port=94.177.187.77:9001
port=*:9001
; (ip_address:port specifier, *:port for all iface)
username=emr ; (default is no username (open server))
password=12345 ; (default is no password (open server))

```

> set up "[inet_http_server]" if necessary

> http run "http://ip:9001"

## Update config changes and run

```
supervisorctl update
supervisorctl start websockets // supervisorctl restart websockets // supervisorctl restart all
supervisorctl status // ex see::  "websockets RUNNING   pid 9072, uptime 0:00:25"
```

> after run; systemctl restart supervisor.service

## Log

```
tail -f /var/log/supervisor/supervisord.log
```

 