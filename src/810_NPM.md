# NPM 

Npm makes it easy for JavaScript developers to share and reuse code, and it makes it easy to update the code that you’re sharing <small><i>
npm, JavaScript geliştiricilerinin kodu paylaşmasını ve yeniden kullanmasını kolaylaştırır ve paylaştığınız kodu güncellemeyi kolaylaştırır</sall></i>

> Notes for Laravel: "Laravel Mix" creates a Web Pack for Laravel application using several common CSS and JavaScript preprocessors. To use "Laravel mix" NPM must be installed. In Laravel, the dependency files are "webpack.mix.js", "package.json" and "composer.json" Packages related to the 'npm install' command in the Laravel project are installed in the 'node_modules' directory.JavaScript's compiled with "npm run dev" in Laravel are created in 'public/js/app.js' and in css '/public/css/app.css'.
 <small><i>"Laravel Mix", birkaç yaygın CSS ve JavaScript ön-işlemcilerini kullanarak Laravel uygulaması için Webpack oluştur. "Laravel mix" i kullanabilmek için NPM'nin kurulu olması gerekir. Laravel'de bağımlılık dosyaları "webpack.mix.js", "package.json" ve "composer.json" dur.Laravel projesinde 'npm install' komutuyla ilgili paketler 'node_modules' dizinine kurulur.Laravel'de "npm run dev" ile derlenen JavaScript'ler '/public/js/app.js' de ve css 'public/css/app.css' de oluşturulur.</small></i>

## npm install

* `node -v` or  `nodejs -v` # check version, install if v8.9.4 does not exist 
* `npm -v`  # check version, install if v5.6.0 does not exist

> If Curl is not installed `sudo apt install curl` 

* `curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -`
* `sudo apt-get install -y nodejs`

> Alternative install: `curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -`  ve  `sudo apt-get install -y nodejs`

```
npm run dev  # js, css compiler
npm run production  # compile and compress (use on project delivery)
npm run watch # automatically compile every time the project is written

npm run serve -- --port 8000 --host 0.0.0.0 ( port and host change)
```

* Npm install examples

```
npm install # Default packages are installed and the 'node_modules' directory is created
npm install bootstrap@4  # with version
npm install vue # with default version 
npm install jquery --save-dev # dev inst.
npm uninstall jquery --save-dev -g  # ( `-g` delete global)
npm update jquery # ubdate jquery
```

> `--save-dev`  # The '--save-dev' parameter adds the installed package to 'package.json

:bulb: When the npm version is upgraded, it is necessary to do `npm rebuild` 

## NPM execute with webpack

* npm  # NPM uses package.json and webpack.mix.js // compilation end is written to 'public/js/app.js' and 'public/css/app.jss'

* webpack  # in ~/webpack.mix.js files

```
const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
```

* Synchronizes the browser; write in ~/webpack.mix.js files

```
mix.browserSync(' add url ');
```

* mix.setPublicPath('../'); # files to be created will go to the parent directory

* `.copyDirectory('public/js', '../public/js')` # The files created will be copied to the parent directory




