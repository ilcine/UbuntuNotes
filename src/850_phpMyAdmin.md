# phpMyAdmin

Using mysql with phpMyAdmin

> _Emrullah İLÇİN_

## Installation for Ubuntu 16.04 with manual:

_installation in the manual_  <small>Install "apt install unzip" if necessary</small>

* `cd /var/www/html`  # go to directory
* `sudo wget https://files.phpmyadmin.net/phpMyAdmin/4.7.8/phpMyAdmin-4.7.8-english.tar.gz`  # download from the site
* `sudo tar xvzf phpMyAdmin-4.7.8-english.tar.gz`  # Open the "tar" package with gunzip.
* `sudo mv phpMyAdmin-4.7.7-english phpmyadmin`  # change name
* `sudo rm phpMyAdmin-4.7.7-english.tar.gz` # delete tar file
* `sudo adduser phpmyadmin`  # create a user
* `sudo chown -R phpmyadmin.phpmyadmin /var/www/html/phpmyadmin`  # give owner
* `cd /var/www/html/phpmyadmin`  # go to "phpmyadmin" directory
* `sudo mkdir config`  # create config directory
* `sudo chmod o+rw config`  # give chmod
* `sudo cp config.sample.inc.php config/config.inc.php` # copy "config.inc.php" to "config" directory.
* `sudo chmod o+w config/config.inc.php` # give chmod to "config.inc.php" 

## Connect to MySql with phpMyAdmin

    ex: http://ilcin.name.tr/phpmyadmin

## Installation for Ubuntu 18
<pre>
apt -y install phpmyadmin php-gettext
    chose apache2
     dbconfig-common? no
</pre>

