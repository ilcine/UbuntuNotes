1) resecue hk
1.1) grup menusunde linux satırı sonuna ilave et

systemd.unit=rescue.target    // f10 ile aç.

mount -o remount,rw /

---
2) boot modları // eski inittab 

listeleme

systemctl list-units --type=target --all | cat

ornek..
```
UNIT                   LOAD      ACTIVE   SUB    DESCRIPTION                  
● all.target             not-found inactive dead   all.target                   
  basic.target           loaded    active   active Basic System                 
  cryptsetup.target      loaded    active   active Local Encrypted Volumes      
  emergency.target       loaded    inactive dead   Emergency Mode               
  getty-pre.target       loaded    inactive dead   Login Prompts (Pre)          
  getty.target           loaded    active   active Login Prompts                
  graphical.target       loaded    active   active Graphical Interface          
  local-fs-pre.target    loaded    active   active Local File Systems (Pre)     
  local-fs.target        loaded    active   active Local File Systems           
  multi-user.target      loaded    active   active Multi-User System            
  network-online.target  loaded    active   active Network is Online            
  network-pre.target     loaded    inactive dead   Network (Pre)                
  network.target         loaded    active   active Network                      
  nss-lookup.target      loaded    active   active Host and Network Name Lookups
  nss-user-lookup.target loaded    active   active User and Group Name Lookups  
  paths.target           loaded    active   active Paths                        
  remote-fs-pre.target   loaded    inactive dead   Remote File Systems (Pre)    
  remote-fs.target       loaded    active   active Remote File Systems          
  rescue.target          loaded    inactive dead   Rescue Mode                  
  shutdown.target        loaded    inactive dead   Shutdown                     
  slices.target          loaded    active   active Slices                       
  sockets.target         loaded    active   active Sockets                      
  sound.target           loaded    active   active Sound Card                   
  swap.target            loaded    active   active Swap                         
  sysinit.target         loaded    active   active System Initialization        
● syslog.target          not-found inactive dead   syslog.target                
  time-sync.target       loaded    active   active System Time Synchronized     
  timers.target          loaded    active   active Timers                       
  umount.target          loaded    inactive dead   Unmount All Filesystems      

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.
```


```


Mapping between runlevels and systemd targets
   ┌─────────┬───────────────────┐
   │Runlevel │ Target            │
   ├─────────┼───────────────────┤
   │0        │ poweroff.target   │
   ├─────────┼───────────────────┤
   │1        │ rescue.target     │
   ├─────────┼───────────────────┤
   │2, 3, 4  │ multi-user.target │
   ├─────────┼───────────────────┤
   │5        │ graphical.target  │
   ├─────────┼───────────────────┤
   │6        │ reboot.target     │
   └─────────┴───────────────────┘

```


recovery mode // komutları

```
$ locate recovery-mode
/lib/recovery-mode
/lib/recovery-mode/l10n.sh
/lib/recovery-mode/options
/lib/recovery-mode/recovery-menu
/lib/recovery-mode/options/apt-snapshots
/lib/recovery-mode/options/clean
/lib/recovery-mode/options/dpkg
/lib/recovery-mode/options/failsafeX
/lib/recovery-mode/options/fsck
/lib/recovery-mode/options/grub
/lib/recovery-mode/options/network
/lib/recovery-mode/options/root
/lib/recovery-mode/options/system-summary
```


default u görme

systemctl get-default 

değiştir

sudo systemctl isolate multi-user.target

refault yap.

sudo systemctl set-default multi-user.target


eski init 5 in grafik mod.
graphical.target


--single mod ethernet yok

lshw -C network  // tanımla veya dmesg|more ile bak

ifconfig enp0s8 up  // or ip link set dev enp0s8 up

ip route show

--

netplan apply  // ile guncell

/etc/netplan/  



