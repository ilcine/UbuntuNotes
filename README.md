# Ubuntu Sunucu Notları

Ubuntu, Linux çekirdeği üzerinde geliştirilen açık kaynak kodlu, 
özgür ve ücretsiz bir işletim sistemidir. Bu notlar Ubuntu 16.04 üzerinde denenmiştir.

> _Emrullah İLÇİN_

* [010 Learn version:](/src/010.Sunucu.Surumunu.Ogrenme.md) 	 
* [020 Update:](/src/020.Guncelleme.md)
* [030 Packet install/remove:](/src/030.Paket.Kurma.ve.Kaldirma.md)
* [040 Service start stop:](/src/040.Servisleri.Durdurma.ve.Baslatma.md)
* [050 Sudo: ](/src/050.Sudo.Yetkisi.md)
* [060 Disk cleanup:](/src/060.tmp.cache.clear.md)
* [070 Using Google Drive:](/src/070.Google-drive.md)
* [080 Ldap & Pam:](/src/080.Ldap.ve.Pam.md)
* [500 LDAP:](/src/ldapServer/README.md)
* [510 DNS:](/src/dnsServer/README.md)
* [700 VNC Server:](/src/700.VncServer.md)
* [710 Using dummy email account in local post:](/src/710.Using_dummy_email_account_in_local_mail.md)
* [720 DHCP Server:](/src/720.dhcp.md)
* [730 Open VPN:](/src/730.openVpn.md)
* [740 Firewall:](/src/740.ufw.firewall.md)
* [750 Networking:](/src/750.networking.md)
* [760 Certbot (let's encrypt):](/src/760.certbot.md)
* [770 Supervisor:](/src/770.supervisor.md)
* [800 Composer:](/src/800_Composer.md)
* [810 NPM:](/src/810_NPM.md)
* [820 Install php on Ubuntu and define in apache2:](/src/820_Install_php.md)
* [830 Apache and Virtual Host:](/src/830_ApacheVirtualHost.md)
* [840 MySql:](/src/840_mysql.md)
* [850 PhpMyAdmin:](/src/850_phpMyAdmin.md)

